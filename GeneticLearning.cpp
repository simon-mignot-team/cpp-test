#include "GeneticLearning.h"
#include <cmath>
#include <cstdlib>

GeneticLearning::GeneticLearning()
{
	//ctor
}

GeneticLearning::~GeneticLearning()
{
	//dtor
}

double _linear(double x)
{
	return x;
}
double _sigmoid(double x)
{
	return 1.d / (1.d + exp(-x));
}

NeuralNetwork GeneticLearning::generateGenome(std::vector<int> layersDefinition)
{
	NeuralNetwork net;
	for(int i = 0; i < layersDefinition[0]; ++i)
		net.addInputNeuron(new Neuron(ActivationFunctions::getFunction("linear"), "Input"));

	for(int l = 1; l < layersDefinition.size() - 1; ++l)
	{
		net.addHiddenLayer();
		for(int i = 0; i < layersDefinition[l]; ++i)
			net.addHiddenNeuron(new Neuron(ActivationFunctions::getFunction("sigmoid"), "Hidden"));
	}

	for(int i = 0; i < layersDefinition[0]; ++i)
		net.addOutputNeuron(new Neuron(ActivationFunctions::getFunction("linear"), "Output"));
	net.connectLayers();
	return net;
}

void GeneticLearning::generateFirstGeneration(std::vector<int> layersDefinition)
{
	//std::cout << "Generate first generation :\n";
	if(layersDefinition.size() < 2)
	{
		std::cout << "Not enough layer.\n";
		return;
	}
	for(int i = 0; i < 10000; ++i)
	{
		NeuralNetScored temp;
		temp.neuralNet = generateGenome(layersDefinition);
		temp.score = 0;
		m_neuralNets.push_back(temp);
	}

	/*for(int i = 0; i < 2; ++i)
	{
		NeuralNetworkData n = m_neuralNets[i].neuralNet.getNeuralNetworkData();
		for(int i = 0; i < n.weights.size(); ++i)
			std::cout << n.weights[i] << '\n';
		std::cout << '\n';
	}*/
//	system("pause");
}
void GeneticLearning::nextGeneration()
{
	m_generation++;
	m_currentNeuralNetID = 0;
}

void GeneticLearning::startLearning()
{
	m_generation = 0;
	m_currentNeuralNetID = 0;
}
bool GeneticLearning::nextNeuralNetExists()
{
	return (m_currentNeuralNetID < m_neuralNets.size());
}
NeuralNetwork GeneticLearning::getNextNeuralNet()
{
	return m_neuralNets[m_currentNeuralNetID++].neuralNet;
}
void GeneticLearning::setCurrentScore(int score)
{
	m_neuralNets[m_currentNeuralNetID].score = score;
}

bool sortNeuralNet(const NeuralNetScored &lhs, const NeuralNetScored &rhs)
{
	return lhs.score > rhs.score;
}

void GeneticLearning::displayScores()
{
	std::sort(m_neuralNets.begin(), m_neuralNets.end(), sortNeuralNet);
	//std::cout << "score : " << m_neuralNets[0].score << '\n';
	int pscore = 0;
	int lastID = -1;
	for(int i = 0; i < m_neuralNets.size(); ++i)
	{
		if(pscore != m_neuralNets[i].score)
		{
			std::cout << lastID+1 << "-" << i << "(" << i + 1 - lastID << ") : " << m_neuralNets[i].score << '\n';
			lastID = i;
		}
		pscore = m_neuralNets[i].score;
	}
}

//void mergeNeuralNet(NeuralNetwork* neuralNet1, NeuralNetwork* neuralNet2);
//void mutateNeuralNet(NeuralNetwork* neuralNet);
