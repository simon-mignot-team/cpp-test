#include <cstdlib>
#include <ctime>

#include <iostream>
#include <cmath>
#include "BackPropagationLearning.h"
#include "GeneticLearning.h"
#include "NeuralNetwork.h"
#include "Neuron.h"
#include "ActivationFunctions.h"

using namespace std;

NeuralNetwork* createNeuralNetwork()
{
	NeuralNetwork* neuralNet = new NeuralNetwork;

	neuralNet->addInputNeuron(new Neuron(ActivationFunctions::getFunction("linear"), "Input", "1"));
	neuralNet->addInputNeuron(new Neuron(ActivationFunctions::getFunction("linear"), "Input", "2"));
	/*neuralNet->addInputNeuron(new Neuron(&linear, "Input", "3"));
	neuralNet->addInputNeuron(new Neuron(&linear, "Input", "4"));*/

	neuralNet->addHiddenLayer();
	neuralNet->addHiddenNeuron(new Neuron(ActivationFunctions::getFunction("sigmoid"), "Hidden1", "1"));
	neuralNet->addHiddenNeuron(new Neuron(ActivationFunctions::getFunction("sigmoid"), "Hidden1", "2"));
	//neuralNet->addHiddenNeuron(new Neuron(&constant, "Hidden1", "3"));
	//neuralNet->addHiddenNeuron(new Neuron(&sigmoid, "Hidden1", "4"));

	/*neuralNet->addHiddenLayer();
	neuralNet->addHiddenNeuron(new Neuron(&sigmoid, "Hidden2", "1"));
	neuralNet->addHiddenNeuron(new Neuron(&sigmoid, "Hidden2", "2"));
	neuralNet->addHiddenNeuron(new Neuron(&sigmoid, "Hidden2", "3"));
	neuralNet->addHiddenNeuron(new Neuron(&sigmoid, "Hidden2", "4"));*/

	neuralNet->addOutputNeuron(new Neuron(ActivationFunctions::getFunction("sigmoid"), "Output", "1"));
	//neuralNet->addOutputNeuron(new Neuron(&sigmoid, "Output", "2"));
	/*neuralNet->addOutputNeuron(new Neuron(&square, "Output", "3"));
	neuralNet->addOutputNeuron(new Neuron(&square, "Output", "4"));*/

	//neuralNet->connectLayers();
	return neuralNet;
}

int main()
{
	srand(time(NULL));

	/*for(int i = 0; i < 10; ++i)
	{
		std::cout << "\n\n\n========= Net" << i << " =========\n";
		NeuralNetwork* neuralNet = createNeuralNetwork();

		/*BackPropagationLearning bpLearning;
		bpLearning.neuralNetwork = neuralNet;
		bpLearning.setLearningValues({{0, 0}, {0, 1}, {1, 0}, {1, 1}},
									 {{1, 1}, {0, 0}, {0, 1}, {1, 0}});

		bpLearning.trainStep();
	}*/
	for(int a = 0; a < 1; ++a)
	{
		GeneticLearning l;
		l.generateFirstGeneration({2, 2, 1});
		l.startLearning();
		//std::cout << "Start learning :\n";
		for(int i = 0; i < 1; ++i) // generation
		{
			//std::cout << "==== Generation " << i << "\n";
			int j = 0;
			while(l.nextNeuralNetExists()) // genomes per generation
			{
				//std::cout << "== Genome " << j++ << "\n";
				NeuralNetwork neuralNet = l.getNextNeuralNet();
				int score = 0;
				double sumerror = 0;
				for(int in = 0; in < 2000; ++in) // game
				{
					LayerNeuronsValues v;
					v.push_back(in);
					v.push_back(in);
					neuralNet.setInput(v);
					double output = neuralNet.activate()[0];
					sumerror += abs(in - output);
					double error = .1d;
					if(output > in - error && output < in + error)
					{
						++score;
						if(score == 2000) std::cout << "\tavgerror:" << sumerror / 2000.d << '\n';
						//if(in > 1000)
							//std::cout << "\nGeneration " << i << " - Genome " << j << "\tin=" << v[0] << "," << v[1] << " - ou=" << output << "\tscore" << score << '\n';
					}
				}
				//std::cout << "score = " << score << "\n\n";
				l.setCurrentScore(score);
				++j;
				if(j % 100 == 0)
					std::cout << j << '\n';
				//system("pause");
			}
			l.nextGeneration();
		}
		std::cout << "\n\n\n=======================\n\n\n";
		l.displayScores();
	}

    return 0;
}
