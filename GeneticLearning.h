#ifndef GENETICLEARNING_H
#define GENETICLEARNING_H

#include <algorithm>

#include "NeuralNetwork.h"

struct NeuralNetScored
{
	NeuralNetwork neuralNet;
	double score;
};

/*

generateFirstGeneration();
startLearning();
{
	{
		getNextNeuralNet();
		# launch the game
		setCurrentScore();
	}
	nextGeneration();
}


*/

class GeneticLearning
{
	public:
		GeneticLearning();
		virtual ~GeneticLearning();

		void generateFirstGeneration(std::vector<int> layersDefinition);
		void nextGeneration();

		NeuralNetwork generateGenome(std::vector<int> layersDefinition);

		void startLearning();
		bool nextNeuralNetExists();
		NeuralNetwork getNextNeuralNet();
		void setCurrentScore(int score);

		void mergeNeuralNet(NeuralNetwork* neuralNet1, NeuralNetwork* neuralNet2);
		void mutateNeuralNet(NeuralNetwork* neuralNet);

		void displayScores();

	protected:

	private:
		std::vector<NeuralNetScored> m_neuralNets; // <neural net, score>

		int m_generation;
		int m_currentNeuralNetID = 0;
};

#endif // GENETICLEARNING_H
