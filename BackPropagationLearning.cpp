#include "BackPropagationLearning.h"

BackPropagationLearning::BackPropagationLearning()
{
	//ctor
}

BackPropagationLearning::~BackPropagationLearning()
{
	//dtor
}

void BackPropagationLearning::setLearningValues(LearningSet inputValues, LearningSet expectedOutputValues)
{
	if(!checkSetsSize(inputValues, expectedOutputValues))
		return;
	m_inputValues = inputValues;
	m_expectedOutputValues = expectedOutputValues;
}

bool BackPropagationLearning::checkSetsSize(LearningSet inputValues, LearningSet expectedOutputValues)
{
	// TODO: check size of every set
	if(neuralNetwork->m_inputs.size() != inputValues[0].size())
	{
		std::cout << "Bad inputs size : neural network=" << neuralNetwork->m_inputs.size() << " ; set size=" << inputValues[0].size() << "\n";
		return false;
	}
	if(neuralNetwork->m_outputs.size() != expectedOutputValues[0].size())
	{
		std::cout << "Bad outputs size : neural network=" << neuralNetwork->m_outputs.size() << " ; set size=" << expectedOutputValues[0].size() << "\n";
		return false;
	}
	return true;
}

double BackPropagationLearning::trainStep()
{
	m_expectedOutputValues.clear();
	for(int i = 0; i < m_inputValues.size(); ++i)
	{
		neuralNetwork->setInput(m_inputValues[i]);
		m_outputValues = neuralNetwork->activate();

		std::cout << "Set " << i << " :\n";
		std::cout << "Input :\n";
		for(std::vector<double>::iterator it = m_inputValues[i].begin(); it != m_inputValues[i].end(); ++it)
			std::cout << *it << '\n';
		std::cout << "Output :\n";
		for(std::vector<double>::iterator it = m_outputValues.begin(); it != m_outputValues.end(); ++it)
			std::cout << *it << '\n';
		std::cout << "\n\n";

		calculateError(m_outputValues, m_expectedOutputValues[i]);
		std::cout << "Error for set " << i << " : " << m_error << "\n\n";
	}
}

void BackPropagationLearning::calculateError(LayerNeuronsValues layer1, LayerNeuronsValues layer2)
{
	LayerNeuronsValues errorValues;
	std::transform(layer1.begin(), layer1.end(),
					layer2.begin(), std::back_inserter(errorValues),
					[&](double a, double b) { return a - b; });
	m_error = std::accumulate(errorValues.begin(), errorValues.end(), (double)0.0d);
}
