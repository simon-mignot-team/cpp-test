#include "Neuron.h"

Neuron::Neuron()
{

}

Neuron::Neuron(std::string layerName, std::string neuronName)
{
	nameNeuron(layerName, neuronName);
}

Neuron::Neuron(ActivationFunction activationFunction, std::string layerName, std::string neuronName) : m_activationFunction(activationFunction)
{
	nameNeuron(layerName, neuronName);
}

Neuron::~Neuron()
{
	//dtor
}

void Neuron::nameNeuron(std::string layerName, std::string neuronName)
{
	static int neuronCount = 0;
	++neuronCount;

	if(neuronName.size() == 0)
		m_neuronName = "Neuron" + std::to_string(neuronCount);
	else
		m_neuronName = neuronName;
	m_layerName = layerName;
}

void Neuron::activate()
{
	if(m_inputConnections.size() > 0) // not input neuron
		calculateInput();
	m_output = m_activationFunction.activationFunction(m_input);
	//std::cout << "calc(" << m_layerName << ";" << m_neuronName << ") : " << m_output << '\n';
	forwardOutput();
}

void Neuron::calculateInput()
{
	m_input = 0;
	for(std::vector<Connection*>::iterator it = m_inputConnections.begin(); it != m_inputConnections.end(); ++it)
		m_input += (*it)->getOutput();
}
void Neuron::forwardOutput()
{
	//std::cout << &m_outputConnections[0] << '\n';
	for(std::vector<Connection*>::iterator it = m_outputConnections.begin(); it != m_outputConnections.end(); ++it)
		(*it)->setInput(m_input);
}
void Neuron::activate(double input)
{
	m_input = input;
	m_output = m_activationFunction.activationFunction(m_input);
}
void Neuron::setInput(double input)
{
	m_input = input;
}

void Neuron::addInputConnection(Connection* connection)
{
	m_inputConnections.push_back(connection);
}

void Neuron::addOutputConnection(Connection* connection)
{
	m_outputConnections.push_back(connection);
}
double Neuron::getOutput() const
{
	return m_output;
}
std::string Neuron::getName()
{
	return m_layerName + "_" + m_neuronName;
}

void Neuron::displayConnections()
{
	std::cout << "Input from :\n";
	for(std::vector<Connection*>::iterator it = m_inputConnections.begin(); it != m_inputConnections.end(); ++it)
		std::cout << '\t' << (*it)->getSender()->getName() << " - weight : " << (*it)->getWeight() << " ; output=" << (*it)->getOutput() << '\n';

	std::cout << "Output to :\n";
	for(std::vector<Connection*>::iterator it = m_outputConnections.begin(); it != m_outputConnections.end(); ++it)
		std::cout << '\t' << (*it)->getReceiver()->getName() << " - weight : " << (*it)->getWeight() << " ; output=" << (*it)->getOutput() << '\n';
}

void Neuron::displayNeuron()
{
	std::cout << getName() << " : input=" << m_input << " ; output=" << m_output << "\n";
	displayConnections();
}
