#ifndef NEURON_H
#define NEURON_H

#include <iostream>
#include <vector>
#include <string>

#include "Connection.h"
#include "ActivationFunctions.h"

class Neuron
{
	public:
		Neuron();
		Neuron(std::string layerName = "layer", std::string neuronName = "");
		Neuron(ActivationFunction activationFunction, std::string layerName = "layer", std::string neuronName = "");
		virtual ~Neuron();

		void activate();
		void activate(double input);
		void calculateInput();
		void forwardOutput();

		void addInputConnection(Connection* connection);
		void addOutputConnection(Connection* connection);

		void setInput(double input);
		double getOutput() const;

		std::string getName();
		void displayConnections();
		void displayNeuron();


	protected:
	private:
		ActivationFunction m_activationFunction;
		double m_input;
		double m_output;

		std::string m_neuronName;
		std::string m_layerName;

		std::vector<Connection*> m_inputConnections;
		std::vector<Connection*> m_outputConnections;

		void nameNeuron(std::string layerName, std::string neuronName);
};

#endif // NEURON_H
