#include "Connection.h"

Connection::Connection(Neuron* sender, Neuron* receiver, double weight) : m_sender(sender), m_receiver(receiver), m_weight(weight)
{

}

Connection::~Connection()
{
	//dtor
}

Neuron* Connection::getSender()
{
	return m_sender;
}
Neuron* Connection::getReceiver()
{
	return m_receiver;
}

double Connection::getWeight() const
{
	return m_weight;
}

double Connection::getOutput() const
{
	return m_input * m_weight;
}

void Connection::setInput(double input)
{
	m_input = input;
}
