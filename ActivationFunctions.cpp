#include "ActivationFunctions.h"

std::map<std::string, ActivationFunction> ActivationFunctions::init_fns()
{
	std::map<std::string, ActivationFunction> tmp_fns;
	tmp_fns["constant"] = { "constant", "f(x) = 1", &ActivationFunctions::fn_constant };
	tmp_fns["linear"] = { "linear", "f(x) = x", &ActivationFunctions::fn_linear };
	tmp_fns["square"] = { "square", "f(x) = x * x", &ActivationFunctions::fn_square };
	tmp_fns["sigmoid"] = { "sigmoid", "f(x) = 1 / (1 + exp(-x))", &ActivationFunctions::fn_sigmoid };
	return tmp_fns;
}

std::map<std::string, ActivationFunction> ActivationFunctions::static_functions = ActivationFunctions::init_fns();

ActivationFunction ActivationFunctions::getFunction(std::string name)
{
	return static_functions[name];
}

double ActivationFunctions::fn_constant(double x)
{
	return 1.0d;
}
double ActivationFunctions::fn_linear(double x)
{
	return x;
}
double ActivationFunctions::fn_square(double x)
{
	return x*x;
}
double ActivationFunctions::fn_sigmoid(double x)
{
	return 1.d / (1.d + exp(-x));
}
