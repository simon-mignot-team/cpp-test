#ifndef CONNECTION_H
#define CONNECTION_H

class Neuron;

class Connection
{
	public:
		Connection(Neuron* sender, Neuron* receiver, double weight);
		~Connection();

		Neuron* getSender();
		Neuron* getReceiver();
		double getWeight() const;

		double getOutput() const;
		void setInput(double input);

	protected:
	private:
	Neuron* m_sender;
	Neuron* m_receiver;
	double m_weight;
	double m_input;
};

#endif // CONNECTION_H
