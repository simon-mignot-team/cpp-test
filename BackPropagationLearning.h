#ifndef BACKPROPAGATIONLEARNING_H
#define BACKPROPAGATIONLEARNING_H

#include <algorithm>
#include <numeric>

#include "NeuralNetwork.h"

typedef std::vector<LayerNeuronsValues> LearningSet;

class BackPropagationLearning
{
	public:
		BackPropagationLearning();
		virtual ~BackPropagationLearning();

		void setLearningValues(LearningSet inputValues, LearningSet expectedOutputValues);
		bool checkSetsSize(LearningSet inputValues, LearningSet expectedOutputValues);

		double trainStep();

		void calculateError(LayerNeuronsValues layer1, LayerNeuronsValues layer2);

	NeuralNetwork* neuralNetwork;

	protected:

	private:
		std::vector<LayerNeuronsValues> m_inputValues;
		std::vector<LayerNeuronsValues> m_expectedOutputValues;
		LayerNeuronsValues m_outputValues;
		double m_error;
};

#endif // BACKPROPAGATIONLEARNING_H
