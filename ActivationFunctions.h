#ifndef ACTIVATIONFUNCTIONS_H
#define ACTIVATIONFUNCTIONS_H

#include <string>
#include <map>
#include <cmath>

struct ActivationFunction
{
	std::string name;
	std::string expression;
	double (*activationFunction)(double);
};

class ActivationFunctions
{
	public:
		static ActivationFunction getFunction(std::string name);
		static std::map<std::string, ActivationFunction> init_fns();

	protected:

	private:
		static std::map<std::string, ActivationFunction> static_functions;

		static double fn_constant(double x);
		static double fn_linear(double x);
		static double fn_square(double x);
		static double fn_sigmoid(double x);
};

#endif // ACTIVATIONFUNCTIONS_H
