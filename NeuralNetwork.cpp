#include "NeuralNetwork.h"

NeuralNetwork::NeuralNetwork()
{

}

NeuralNetwork::~NeuralNetwork()
{
	//dtor
}

void NeuralNetwork::addInputNeuron(Neuron* neuron)
{
    m_inputs.push_back(neuron);
}
void NeuralNetwork::addOutputNeuron(Neuron* neuron)
{
	m_outputs.push_back(neuron);
}
bool NeuralNetwork::addHiddenNeuron(Neuron* neuron, int hiddenLayerID)
{
	if(hiddenLayerID == -1)
		hiddenLayerID = m_hiddens.size() - 1;
	if(hiddenLayerID >= m_hiddens.size())
		return false;
	m_hiddens[hiddenLayerID].push_back(neuron);
	return true;
}

void NeuralNetwork::addHiddenLayer()
{
	m_hiddens.push_back(NeuronsLayer());
}

bool NeuralNetwork::addNeuron(LayerType layerType, Neuron* neuron, int hiddenLayerID)
{
	if(layerType == INPUT)
		addInputNeuron(neuron);
	else if(layerType == HIDDEN)
		return addHiddenNeuron(neuron, hiddenLayerID);
	else if(layerType == OUTPUT)
		addOutputNeuron(neuron);
	return true;
}

void NeuralNetwork::connect(NeuronsLayer *senders, NeuronsLayer *receivers)
{
	for(NeuronsLayer::iterator sendersIterator = senders->begin(); sendersIterator != senders->end(); ++sendersIterator)
	{
		for(NeuronsLayer::iterator receiversIterator = receivers->begin(); receiversIterator != receivers->end(); ++receiversIterator)
		{
			double weight = (rand() % 20000 - 10000) / 10000.d;
			Connection* temp = new Connection(*sendersIterator, *receiversIterator, weight);

			m_connections.push_back(temp);
			m_neuralNetworkData.weights.push_back(weight);

			(*sendersIterator)->addOutputConnection(temp);
			(*receiversIterator)->addInputConnection(temp);
		}
	}
}
void NeuralNetwork::connectLayers()
{
	if(m_hiddens.size() == 0)
		std::cout << "No hidden layer !\n";
	connect(&m_inputs, &m_hiddens[0]);
	for(std::vector<NeuronsLayer>::iterator it = m_hiddens.begin() + 1; it != m_hiddens.end(); ++it)
		connect(&(*(it - 1)), &(*it));
	connect(&m_hiddens[m_hiddens.size() - 1], &m_outputs);
}

void NeuralNetwork::setInput(LayerNeuronsValues inputValues)
{
	if(inputValues.size() < m_inputs.size())
		return;
	for(int i = 0; i < m_inputs.size(); ++i)
		m_inputs[i]->setInput(inputValues[i]);
}

void NeuralNetwork::activateLayer(NeuronsLayer* layer)
{
	if(layer->size() == 0)
		std::cout << "No neuron in this layer !\n";
	for(NeuronsLayer::iterator it = layer->begin(); it != layer->end(); ++it)
		(*it)->activate();
}

LayerNeuronsValues NeuralNetwork::getOutputsValues()
{
	LayerNeuronsValues values;
	for(NeuronsLayer::iterator it = m_outputs.begin(); it != m_outputs.end(); ++it)
		values.push_back((*it)->getOutput());
	return values;
}

LayerNeuronsValues NeuralNetwork::activate()
{
	activateLayer(&m_inputs);
	for(std::vector<NeuronsLayer>::iterator it = m_hiddens.begin(); it != m_hiddens.end(); ++it)
		activateLayer(&(*it));
	activateLayer(&m_outputs);
	return getOutputsValues();
}

NeuralNetworkData NeuralNetwork::getNeuralNetworkData()
{
	return m_neuralNetworkData;
}
