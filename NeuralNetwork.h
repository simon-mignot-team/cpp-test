#ifndef NEURALNETWORK_H
#define NEURALNETWORK_H

#include <vector>

#include "Neuron.h"

typedef std::vector<Neuron*> NeuronsLayer;
typedef std::vector<double> LayerNeuronsValues;

enum LayerType
{
	INPUT = 0, HIDDEN, OUTPUT
};

struct NeuralNetworkData
{
	std::vector<double> weights;
};

class NeuralNetwork // feed-forward type
{
	friend class BackPropagationLearning;
	public:
		NeuralNetwork();
		virtual ~NeuralNetwork();

		void addInputNeuron(Neuron* neuron);
		void addOutputNeuron(Neuron* neuron);
		bool addHiddenNeuron(Neuron* neuron, int hiddenLayerID = -1);

		void addHiddenLayer();

		bool addNeuron(LayerType layerType, Neuron* neuron, int hiddenLayerID = -1);

		void connectLayers();
		NeuralNetworkData getNeuralNetworkData();

		void setInput(LayerNeuronsValues inputValues);
		LayerNeuronsValues activate();
		LayerNeuronsValues getOutputsValues();

	protected:

	private:
	void connect(NeuronsLayer *senders, NeuronsLayer *receivers);
	void activateLayer(NeuronsLayer* layer);

		NeuronsLayer m_inputs;
		std::vector<NeuronsLayer> m_hiddens;
		NeuronsLayer m_outputs;
		NeuralNetworkData m_neuralNetworkData;
		std::vector<Connection*> m_connections;
};

#endif // NEURALNETWORK_H
